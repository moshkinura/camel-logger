const { Logger } = require('../../lib/index');
const logger = new Logger('log/api_log.txt');

async function startTest(): Promise<void> {
  await logger.group('Group log');
  await logger.line();
  await logger.log('Log .log');
  await logger.text('Log .text');
  await logger.delimiter();
  await logger.groupEnd('Group log');
  // logger.groupend('Group log') // alias

  // ----- [GROUP]: Group log -----
  // [2022-06-09T00:38:58.862Z][L]: -------------------------
  // [2022-06-09T00:38:58.865Z][L]: Log .log
  // [2022-06-09T00:38:58.866Z][L]: Log .text
  // [2022-06-09T00:38:58.869Z][L]: -------------------------
  // ----- [GROUPEND]: Group log -----

  await logger.line();
  await logger.delimiter(); // alias
  await logger.separator(); // alias
  // [2022-06-09T00:38:58.871Z][L]: -------------------------
  // [2022-06-09T00:38:58.872Z][L]: -------------------------
  // [2022-06-09T00:38:58.874Z][L]: -------------------------

  await logger.empty();
  await logger.blank();
  await logger.null();
  //
  //
  //

  await logger.log('Log .log');
  await logger.logging('Log .log'); // alias
  await logger.text('Log .log'); // alias
  await logger.l('Log .log'); // alias
  // [2022-06-09T00:38:58.881Z][L]: Log .log
  // [2022-06-09T00:38:58.882Z][L]: Log .log
  // [2022-06-09T00:38:58.884Z][L]: Log .log
  // [2022-06-09T00:38:58.885Z][L]: Log .log

  await logger.info('Log .info');
  await logger.information('Log .info'); // alias
  await logger.i('Log .info'); // alias
  // [2022-06-09T00:38:58.887Z][I]: Log .info
  // [2022-06-09T00:38:58.888Z][I]: Log .info
  // [2022-06-09T00:38:58.889Z][I]: Log .info

  await logger.error('Log .error');
  await logger.err('Log .error');
  await logger.e('Log .error');
  // [2022-06-09T00:38:58.890Z][E]: Log .error
  // [2022-06-09T00:38:58.928Z][E]: Log .error
  // [2022-06-09T00:38:58.931Z][E]: Log .error

  const json = {
    strict: { column: { age: 24, name: 'Yury', job: 'Developer' } },
  }; // logger.dev, logger.json
  await logger.dev('Log .dev'); // No record log file
  await logger.developer('Log .dev'); // No record log file / alias
  await logger.d('Log .dev'); // No record log file / alias
  // [2022-06-09T00:38:58.933Z][D]: Log .dev
  // [2022-06-09T00:38:58.933Z][D]: Log .dev
  // [2022-06-09T00:38:58.934Z][D]: Log .dev

  await logger.dev(json, true); // true - on mode JSON.STRINGIFY / No record log file
  await logger.developer(json, true); // true - on mode JSON.STRINGIFY / No record log file / alias
  await logger.d(json, true); // true - on mode JSON.STRINGIFY / No record log file / alias
  // [2022-06-09T00:38:58.934Z][D]: {
  //     "strict": {
  //             "column": {
  //                     "age": 24,
  //                     "name": "Yury",
  //                     "job": "Developer"
  //             }
  //     }
  // }
  // [2022-06-09T00:38:58.936Z][D]: {
  //     "strict": {
  //             "column": {
  //                     "age": 24,
  //                     "name": "Yury",
  //                     "job": "Developer"
  //             }
  //     }
  // }
  // [2022-06-09T00:38:58.938Z][D]: {
  //     "strict": {
  //             "column": {
  //                     "age": 24,
  //                     "name": "Yury",
  //                     "job": "Developer"
  //             }
  //     }
  // }
  // [2022-06-09T00:38:58.940Z][JSON]: {
  //     "strict": {
  //             "column": {
  //                     "age": 24,
  //                     "name": "Yury",
  //                     "job": "Developer"
  //             }
  //     }
  // }

  await logger.json(json); // No record log file
  await logger.stringify(json); // No record log file / alias
  // [2022-06-09T00:38:58.940Z][JSON]: {
  //     "strict": {
  //             "column": {
  //                     "age": 24,
  //                     "name": "Yury",
  //                     "job": "Developer"
  //             }
  //     }
  // }
  // [2022-06-09T00:38:58.942Z][JSON]: {
  //     "strict": {
  //             "column": {
  //                     "age": 24,
  //                     "name": "Yury",
  //                     "job": "Developer"
  //             }
  //     }
  // }
}

startTest();
