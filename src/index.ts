import * as fs from 'fs-extra';
import * as path from 'path';

import { Type, TypeConsole } from './types';

export class Logger {
  file: string;
  maxMemLog: number;
  allLog: string[];

  constructor(file: string, maxMemLog: number = 5000) {
    this.file = path.join(file);
    this.maxMemLog = maxMemLog - 1;
    this.allLog = [];
  }

  get all(): string[] {
    return this.allLog;
  }

  get _date(): string {
    return new Date().toISOString();
  }

  _bodyMessage(text: string, type: Type): string {
    const body: string = `[${this._date}][${type}]`;
    const message: string = `${body}: ${text}`;
    return message;
  }

  _bodyGroup(
    text: string,
    type: Type.G | Type.GROUP | Type.GE | Type.GROUPEND,
  ): string {
    const message: string = `----- [${type}]: ${text} -----`;
    return message;
  }

  _writeConsole(text: string, type: TypeConsole = TypeConsole.L): void {
    console[type](text);
  }

  // _pushAllLog(data) {
  //   this.allLog.push(data);

  //   if (this.allLog.length >= this.maxMemLog) {
  //     this.allLog.splice(0, this.allLog.length - 1 - this.maxMemLog);
  //   }
  // }

  async _writeFile(message: string): Promise<boolean> {
    try {
      // this._pushAllLog(message)

      const exists = await fs.pathExists(this.file);

      if (!exists) {
        await fs.outputFile(this.file, `${message}\n`);
      } else {
        await fs.appendFile(this.file, `${message}\n`);
      }
      return true;
    } catch (e) {
      console.error(e);
      return false;
    }
  }

  async group(text: string): Promise<void> {
    // Group Log
    const message: string = this._bodyGroup(text, Type.GROUP);
    this._writeConsole(message, TypeConsole.GROUP);

    await this._writeFile(message);
  }

  async groupEnd(text: string): Promise<void> {
    const message: string = this._bodyGroup(text, Type.GROUPEND);
    this._writeConsole(message);
    this._writeConsole('', TypeConsole.GROUPEND);

    await this._writeFile(message);
  }

  async groupend(text: string): Promise<void> {
    await this.groupEnd(text);
  }

  async line(): Promise<void> {
    const text: string = '-------------------------';
    const message: string = this._bodyMessage(text, Type.L);
    this._writeConsole(message);

    await this._writeFile(message);
  }

  async delimiter(): Promise<void> {
    await this.line();
  }

  async separator(): Promise<void> {
    await this.line();
  }

  async empty(): Promise<void> {
    const message: string = '';
    this._writeConsole(message);

    await this._writeFile(message);
  }

  async blank(): Promise<void> {
    await this.empty();
  }

  async null(): Promise<void> {
    await this.empty();
  }

  async space(): Promise<void> {
    await this.empty();
  }

  async log(text: string): Promise<void> {
    const message: string = this._bodyMessage(text, Type.L);
    this._writeConsole(message);

    await this._writeFile(message);
  }

  async logging(text: string): Promise<void> {
    await this.log(text);
  }

  async text(text: string): Promise<void> {
    await this.log(text);
  }

  async l(text: string): Promise<void> {
    await this.log(text);
  }

  async info(text: string): Promise<void> {
    const message: string = this._bodyMessage(text, Type.I);
    this._writeConsole(message);

    await this._writeFile(message);
  }

  async information(text: string): Promise<void> {
    await this.info(text);
  }

  async i(text: string): Promise<void> {
    await this.info(text);
  }

  async error(text: string): Promise<void> {
    const message: string = this._bodyMessage(text, Type.E);
    this._writeConsole(message);

    await this._writeFile(message);
  }

  async err(text: string): Promise<void> {
    await this.error(text);
  }

  async e(text: string): Promise<void> {
    await this.error(text);
  }

  async dev(text: any, json: boolean = false): Promise<void> {
    text = json ? JSON.stringify(text, null, '\t') : text;
    const message: string = this._bodyMessage(text, Type.D);
    this._writeConsole(message);
  }

  async developer(text: any, json: boolean = false): Promise<void> {
    await this.dev(text, json);
  }

  async d(text: any, json: boolean = false): Promise<void> {
    await this.dev(text, json);
  }

  async json(text: any): Promise<void> {
    text = text ? JSON.stringify(text, null, '\t') : '';
    const message: string = this._bodyMessage(text, Type.JSON);
    this._writeConsole(message);
  }

  async stringify(text: any): Promise<void> {
    await this.json(text);
  }
}
